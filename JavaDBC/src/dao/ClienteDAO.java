
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Cliente;

public class ClienteDAO {
    private DataSource dataSource;
    
    public ClienteDAO(DataSource dataSource){
        this.dataSource = dataSource;
    }
    
    public ArrayList<Cliente>readAll(){
        try{
            String SQL = "SELECT * from clientes";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();
            
            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            while(rs.next()){
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
        }
            ps.close();
            return lista;
        }
        catch(SQLException ex){
            System.out.println("Erro ao recuperar" +ex);
        }
        return null;
    }
    public void inserir(String nome, String email, String telefone) {
        try {
            String comando = "INSERT INTO clientes (nome, email, telefone) VALUES ('" + nome + "', '" + email + "', " + telefone + ")";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(comando);
            ps.executeUpdate(comando);
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso");
            ps.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir" + e.getMessage());
        }
    }
    public void remover(int id) {
        try {
            String comando = "DELETE FROM clientes WHERE id=" + id;
            PreparedStatement ps = dataSource.getConnection().prepareStatement(comando);
            ps.executeUpdate(comando);
            ps.executeUpdate(comando);
            JOptionPane.showMessageDialog(null, "Dado removido com sucesso");
            ps.close();

        } catch (SQLException e) {
            System.out.println("Erro ao remover");
        }
    }
}
